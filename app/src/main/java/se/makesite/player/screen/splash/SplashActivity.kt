package se.makesite.player.screen.splash

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import se.makesite.player.screen.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        MainActivity.start(this)
        finish()
    }
}
