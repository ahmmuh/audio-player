package se.makesite.player.screen

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.category_row.view.*
import se.makesite.player.R

class CategoryAdapter(val categories: List<String>): RecyclerView.Adapter<CategoryAdapter.ViewHolder> (){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.category_row, parent, false)


        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

     holder.categoryName.text = categories[position]

    }

    override fun getItemCount(): Int {

        return categories.size
    }



    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView ) {

        val categoryName  : TextView = itemView.categoryName

    }


}
