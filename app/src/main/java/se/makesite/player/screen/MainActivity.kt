package se.makesite.player.screen

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log.d
import com.google.gson.Gson
import kotlinx.android.synthetic.main.main_activity.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import se.makesite.player.Books
import se.makesite.player.R
import java.net.URL

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

   doAsync {

       val json = URL("https://finepointmobile.com/data/products.json").readText()
     uiThread {
         d("Ahmed","hämtar json file $json")

         val books = Gson().fromJson(json, Array<Books>:: class.java).toList()
     }

   }

        RecyclerVew_id.layoutManager = LinearLayoutManager(this)
       //RecyclerVew_id.adapter = CategoryAdapter(categories = )


    }

    companion object {
        fun start(context: Context){
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }



}
